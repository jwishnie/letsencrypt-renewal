Scripts for renewing Let's Encrypt certs for GitLab hosted pages.

Based on Bas Harenslak's shell scripts:
https://www.harenslak.nl/blog/https-letsencrypt-gitlab-hugo/

Re-written in Python to better escape edge-cases beacuse I will _never_ 
grok BASH escaping! 
