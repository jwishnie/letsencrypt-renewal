#!/usr/bin/env python3

import subprocess, urllib.request, urllib.error
from os import environ, makedirs
from time import sleep
import sys

env_vars = [
    'CI_PROJECT_DIR', 'CERTBOT_VALIDATION', 'CERTBOT_TOKEN',
    'CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN', 'CI_PROJECT_NAME',
    'CI_PROJECT_NAMESPACE', 'GITLAB_USER_LOGIN'
    ]

def print_flush(msg):
    print(msg)
    sys.stdout.flush()

if __name__ == "__main__":
    # check env vars
    for var in env_vars:
        if var not in environ:
            print_flush("Environment variable '{}' not found. Exiting.".format(var))
            exit(1)

    # make directory for the challenge
    docroot = (environ['VERIFICATION_DOCROOT'] if 'VERIFICATION_DOCROOT' in environ else None)

    challenge_dir = '{}/{}.well-known/acme-challenge'.format(environ['CI_PROJECT_DIR'],
                                                             (docroot+'/' if docroot is not None else ''))
    makedirs(challenge_dir, exist_ok=True)

    challenge_file = '{}/{}'.format(challenge_dir, environ['CERTBOT_TOKEN'])
    print_flush("Challenge file: "+challenge_file)

    # write challenge
    with open(challenge_file, 'w') as f:
        f.write(environ['CERTBOT_VALIDATION'])

    # add to git and check in
    subprocess.run(['git', 'add', challenge_file])
    subprocess.run(['git', 'commit', '-m', 'Added Certbot challenge file for cert renewal'])

    # repo url with login
    repo = 'https://{}:{}@gitlab.com/{}/{}.git'.format(environ['GITLAB_USER_LOGIN'],
                                                       environ['CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN'],
                                                       environ['CI_PROJECT_NAMESPACE'],
                                                       environ['CI_PROJECT_NAME'])
    print_flush(repo)
    subprocess.run(['git', 'push', repo, 'HEAD:master'])

    # now loop waiting for page to be published
    max_tries = 80
    interval_sec = 15
    check_url = 'http://{}.gitlab.io/{}/.well-known/acme-challenge/{}'.format(environ['CI_PROJECT_NAMESPACE'],
                                                                               environ['CI_PROJECT_NAME'],
                                                                               environ['CERTBOT_TOKEN'])

    print_flush(check_url)

    for i in range(max_tries):
        try:
            with urllib.request.urlopen(check_url) as f:
                if f.getcode() == 200:
                    # success
                    print_flush('authorized')
                    exit(0)
        except urllib.error.HTTPError:
            pass

        sleep(interval_sec)

    exit(2)
