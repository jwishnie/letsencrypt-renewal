#!/usr/bin/env python3

import subprocess, sys
from os import environ

env_vars = [
    'CI_PROJECT_DIR', 'CERTBOT_TOKEN',
    'CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN', 'CI_PROJECT_NAME',
    'CI_PROJECT_NAMESPACE', 'GITLAB_USER_LOGIN'
    ]

def print_flush(msg):
    print(msg)
    sys.stdout.flush()

if __name__ == "__main__":
    # check env vars
    for var in env_vars:
        if var not in environ:
            print_flush("Environment variable '{}' not found. Exiting.".format(var))
            exit(1)

    # make directory for the challenge
    challenge_dir = '{}/public/.well-known/acme-challenge'.format(environ['CI_PROJECT_DIR'])
    challenge_file = '{}/{}'.format(challenge_dir, environ['CERTBOT_TOKEN'])

    print_flush('Challenge file: '+challenge_file)
    # remove challenge file
    subprocess.run(['git', 'rm', challenge_file])
    subprocess.run(['git', 'commit', '-m', 'Removed Certbot challenge file'])

    # repo url with login
    repo = 'https://{}:{}@gitlab.com/{}/{}.git'.format(environ['GITLAB_USER_LOGIN'],
                                                       environ['CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN'],
                                                       environ['CI_PROJECT_NAMESPACE'],
                                                       environ['CI_PROJECT_NAME'])

    print_flush("repo: "+repo)

    subprocess.run(['git', 'push', repo, 'HEAD:master'])

    exit(0)
