#!/usr/bin/env python3

import ssl, socket, argparse, subprocess, urllib.request, urllib.parse
from os import environ, putenv, makedirs
from datetime import datetime
import sys
from dateutil.parser import parse as parse_datetime  # from python-dateutil
from dateutil.tz import tzutc

LETS_ENCRYPT_ETC_DIR = '/etc/letsencrypt'
CHAIN = 'fullchain.pem'
KEY = 'privkey.pem'
GITLAB_REQ = 'https://gitlab.com/api/v4/projects/{}/pages/domains/{}'
LETS_ENCRYPT_LOGS_DIR = '/var/log/letsencrypt'


env_vars = [
    'CI_PROJECT_ID', 'GITLAB_USER_EMAIL',
    'CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN'
    ]

def print_flush(msg):
    print(msg)
    sys.stdout.flush()

def get_cert(hostname):
    context = ssl.create_default_context()
    connection = context.wrap_socket(socket.socket(socket.AF_INET), server_hostname=hostname)
    connection.connect((hostname, 443))
    return connection.getpeercert()


if __name__ == "__main__":
    # check env vars
    for var in env_vars:
        if var not in environ:
            print_flush("Environment variable '{}' not found. Exiting.".format(var))
            exit(1)

    proj_id = environ['CI_PROJECT_ID']
    token = environ['CERTBOT_RENEWAL_PIPELINE_GIT_TOKEN']
    email = environ['GITLAB_USER_EMAIL']

    # Parse arguments
    aparser = argparse.ArgumentParser(description="Generate Let's Encrypt Cert.")
    aparser.add_argument('-n', '--hostname', action='append', required=True,
                         help="Name of hosts who's cert you want to generate or renew")
    aparser.add_argument('-t', '--time-to-expiry', default=30, type=int, help='Time in days left until cert expires')
    aparser.add_argument('-e', '--email', default=email)
    aparser.add_argument('-r', '--docroot', default='public',
                         help='artifact folder for deployed pages. where to place verification files')
    aparser.add_argument('-l', '--log_dir', default=LETS_ENCRYPT_LOGS_DIR)
    aparser.add_argument('-c', '--etc_dir', default=LETS_ENCRYPT_ETC_DIR)
    args = aparser.parse_args()

    LETS_ENCRYPT_LIVE_DIR = args.etc_dir + '/live'
    LETS_ENCRYPT_ARCHIVE_DIR = args.etc_dir + '/archive'

    log_dir = args.log_dir

    # set up directories for certbot
    makedirs(log_dir, exist_ok=True)
    makedirs(LETS_ENCRYPT_LIVE_DIR, exist_ok=True)

    # see if cert exists and has less that provide time-to-expiry
    print_flush("Checking for existing cert...")
    cert = None
    try:
        cert = get_cert(args.hostname[0])
    except OSError:
        # can't pull cert, maybe SSL not active, like first time set-up, we'll keep going
        pass
    except ssl.CertificateError:
        # assume case where no cert set-up so is returning gitlab.io
        # add and continue
        pass

    if cert is not None:
        # Check if within the time-to-expiry
        print_flush("Cert found. Checking expiry...")
        expiry = parse_datetime(cert['notAfter'])
        if (expiry - datetime.now(tzutc())).days > args.time_to_expiry:
            print_flush("Cert for '"+args.hostname[0]+"' valid for more than provided time-to-expiry of " +
                  str(args.time_to_expiry) + " days.")
            exit(0)

    # now lets generate the cert!
    print_flush("Cert expires sooner than selected expiry, regenerating...")
    certbot_args = ['certbot', 'certonly', '--manual', '--debug',
                    '--non-interactive',
                    '--preferred-challenges=http', '-m', args.email,
                    '--agree-tos', '--manual-auth-hook', 'letsencrypt_authenticator.py',
                    '--manual-cleanup-hook', 'letsencrypt_cleanup.py',
                    '--manual-public-ip-logging-ok', '--logs-dir',
                    log_dir]

    # a little list weirdness to handle multiple domain arguments
    for host in args.hostname:
        certbot_args.append('-d')
        certbot_args.append(host)

    cp = None

    # set up the env for the sub-process
    putenv('VERIFICATION_DOCROOT',  args.docroot)
    try:
        print_flush("Running Certbot")
        cp = subprocess.run(certbot_args, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as err:
        print_flush("Certbot failed to execute. Exiting.")
        print_flush('Error:\n{}\nOut:\n{}\n'.format(err.stderr, err.stdout))

        try:
            with open(log_dir+'/letsencrypt.log', 'r') as f:
                print_flush('Log:\n')
                for line in f:
                    print_flush('')
        except:
            pass
        exit(5)

    # Set up the form data
    data = dict()
    with open(LETS_ENCRYPT_LIVE_DIR + '/' + args.hostname[0] + '/' + CHAIN, 'r') as f:
        data['certificate'] = f.read()
    with open(LETS_ENCRYPT_LIVE_DIR + '/' + args.hostname[0] + '/' + KEY, 'r') as f:
        data['key'] = f.read()

    data = urllib.parse.urlencode(data)
    data = data.encode('ascii')

    # post chain and key for each domain in the project
    for domain in args.hostname:
        print_flush('Installing Cert for domain: ' + domain)
        req = urllib.request.Request(GITLAB_REQ.format(proj_id, domain), method='PUT', data=data)
        req.add_header('PRIVATE-TOKEN', token)
        try:
            with urllib.request.urlopen(req) as f:
                # could log results, but no need
                pass
        except:
            print_flush('Could not install cert for domain: ' + domain)
            continue
        print_flush('Cert installed/renewed for domain: ' + domain)
